package main

import (
	"bufio"
	"bytes"
	"codeberg.org/Rinma/vcs-profile-html-generator/types"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
)

func createJSONQueryString(queryString string) string {
	return fmt.Sprintf("{\"query\": \"%s\"}", queryString)
}

func closeFile(file *os.File) {
	err := file.Close()
	if err != nil {
		panic(err)
	}
}

func loadGitHubQuery(queryPath string) (string, error) {
	file, err := os.Open(queryPath)
	if err != nil {
		return "", err
	}
	defer closeFile(file)

	var lines string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines += scanner.Text()
	}

	lines = createJSONQueryString(lines)
  
    fmt.Println("Query:", lines)

	return lines, scanner.Err()
}

func bodyClose(resp *http.Response) {
	err := resp.Body.Close()

	if err != nil {
		panic(err)
	}
}

func callApi(githubToken *string) ([]byte, error) {
	var c = &http.Client{}
	var githubApiUrl = "https://api.github.com/graphql"
	query, _ := loadGitHubQuery("data/github-query.graphql")
	req, _ := http.NewRequest("POST", githubApiUrl, bytes.NewBuffer([]byte(query)))
	var tokenHeader = fmt.Sprintf("bearer %s", *githubToken)
	req.Header.Set("Authorization", tokenHeader)
	resp, _ := c.Do(req)
	defer bodyClose(resp)
	body, err := ioutil.ReadAll(resp.Body)

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	fmt.Println("response Body:", string(body))

	return body, err
}

func main() {
	var githubToken = flag.String("github_api_token", "123456789", "Token to connect to the GitHup Api")
	flag.Parse()

	body, _ := callApi(githubToken)

	var dat = types.GitHubData{}
	if err := json.Unmarshal(body, &dat); err != nil {
		panic(err)
	}

	f, _ := os.Create("vcs-repos.html")
	tmpl, _ := template.ParseFiles("templates/github.html")
	err := tmpl.Execute(f, dat.Data.Viewer)
	if err != nil {
		panic(err)
	}
	defer closeFile(f)
}
