package types

type Stargazers struct {
	TotalCount int
}

type Language struct {
	Name  string
	Color string
}

type RepoInfo struct {
	Name            string
	NameWithOwner   string
	Url             string
	HomepageUrl     string
	Description     string
	ForkCount       int
	Stargazers      Stargazers
	PrimaryLanguage Language
}

type Repos struct {
	TotalCount int
	Nodes      []RepoInfo
}

type Followers struct {
	TotalCount int
}

type Issues struct {
	TotalCount int
}

type OrgInfo struct {
	Name     string
	Location string
}

type Organization struct {
	Nodes []OrgInfo
}

type ViewerData struct {
	Login               string
	Name                string
	Location            string
	Bio                 string
	WebsiteUrl          string
	AvatarUrl           string
	Company             string
	Organizations       Organization
	Followers           Followers
	Following           Followers
	PinnedRepositories  Repos
	StarredRepositories Repos
	Projects            Repos
	Repositories        Repos
	Issues              Issues
}

type DataData struct {
	Viewer ViewerData
}

type GitHubData struct {
	Data DataData
}
